# NomDict

Example dictionary command line app in Elixir.

## Setup

Setup database and other details inside of `config/prod.exs`.
Then proceed to compile and build escript:

```bash
$ MIX_ENV=prod mix do deps.get, ecto.setup, escript.build
```

## Use
After building escript, then use the command line to search for
a word:

```bash
$ ./nom_dict apple
apple:
Definition 1: delicious
Example 1: Eat this delicious apple.
```

