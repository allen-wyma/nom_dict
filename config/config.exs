# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :nom_dict, ecto_repos: [NomDict.Repo]

config :nom_dict, :fetcher, NomDict.Fetcher.FetcherMock

import_config "#{Mix.env()}.exs"
