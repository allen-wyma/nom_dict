use Mix.Config

config :nom_dict, NomDict.Repo,
  username: "postgres",
  password: "postgres",
  database: "nom_dict_dev",
  hostname: "localhost",
  pool_size: 10
