use Mix.Config

# Print only warnings and errors during prod
config :logger, level: :warn

config :nom_dict, NomDict.Repo,
  username: "postgres",
  password: "postgres",
  database: "nom_dict_prod",
  pool_size: 20
