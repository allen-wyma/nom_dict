use Mix.Config

config :nom_dict, :fetcher, NomDict.Fetcher.FetcherMock

# Print only warnings and errors during test
config :logger, level: :warn

config :nom_dict, NomDict.Repo,
  username: "postgres",
  password: "postgres",
  database: "nom_dict_test",
  hostname: System.get_env("DB_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
