defmodule NomDict.Fetcher.Fetcher do
  @callback fetch(String.t()) :: {:ok, %NomDict.Dictionary.Word{}} | {:error, any()}
end
