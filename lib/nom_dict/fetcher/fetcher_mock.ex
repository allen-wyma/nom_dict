defmodule NomDict.Fetcher.FetcherMock do
  alias NomDict.Dictionary.{Word, WordDefinition}
  @behaviour NomDict.Fetcher.Fetcher

  @impl NomDict.Fetcher.Fetcher
  def fetch("apple") do
    {:ok,
     %Word{
       token: "apple",
       antonyms: ["not apple"],
       synonyms: ["same apple"],
       definitions: [
         %WordDefinition{definition: "delicious", example: "Eat this delicious apple."}
       ]
     }}
  end

  def fetch("agree") do
    {:ok,
      %Word{
        token: "agree",
        antonyms: ["disagree"],
        synonyms: ["concur"],
        definitions: [
          %WordDefinition{definition: "to have consensus", example: "So happy you agree."}
        ]
      }}
  end

  def fetch(_), do: {:error, :not_found}
end
