defmodule NomDict.CLI do
  alias NomDict.Dictionary

  @spec main([any()]) :: :ok
  def main([]) do
    IO.puts("Welcome to NomDict!")
    IO.puts("Please use './nom_dict' and a word to search for its definition.")
  end

  def main([word]) do
    {:ok, found_word} = Dictionary.search(word)
    IO.puts("#{word}:")
    found_word.definitions
    |> Stream.with_index(1)
    |> Enum.each(fn {d, i} -> IO.puts("Definition #{i}: #{d.definition}\nExample #{i}: #{d.example}") end)
  end

  def main([_word | _]), do: IO.puts("Please search for just 1 word.")
end