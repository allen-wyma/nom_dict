defmodule NomDict.Repo do
  use Ecto.Repo,
    otp_app: :nom_dict,
    adapter: Ecto.Adapters.Postgres
end
