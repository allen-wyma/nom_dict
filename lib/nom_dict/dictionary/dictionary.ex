defmodule NomDict.Dictionary do
  import Ecto.Query
  alias NomDict.{Dictionary.Word, Repo}
  @fetcher Application.get_env(:nom_dict, :fetcher)

  @spec search(String.t()) :: {:ok, %Word{}} | {:error, any()}
  def search(word) do
    case find_local_word(word) do
      nil ->
        word
        |> @fetcher.fetch()
        |> handle_response()

      word ->
        {:ok, word}
    end
  end

  @spec find_local_word(String.t()) :: %Word{} | nil
  defp find_local_word(word), do: Repo.one(from(w in Word, where: w.token == ^word))

  @spec handle_response({:ok, %Word{}} | {:error, any()}) :: {:ok, %Word{}} | {:error, any()}
  defp handle_response({:ok, %Word{} = word}), do: create_word(word)

  defp handle_response({:error, _reason} = error_tuple), do: error_tuple

  @spec create_word(%Word{}, %{}) :: {:ok, %Word{}} | {:error, %Ecto.Changeset{}}
  def create_word(word, attrs \\ %{}) do
    word
    |> Word.changeset(attrs)
    |> Repo.insert()
  end
end
