defmodule NomDict.Dictionary.Word do
  use Ecto.Schema
  import Ecto.Changeset
  alias NomDict.Dictionary.WordDefinition

  schema "words" do
    field(:token, :string, null: false)
    embeds_many(:definitions, WordDefinition)
    field(:antonyms, {:array, :string}, default: [])
    field(:synonyms, {:array, :string}, default: [])
    field(:data, :map, default: %{})
    timestamps()
  end

  @spec changeset(
          Ecto.Schema.t(),
          %{required(binary()) => term()} | %{required(atom()) => term()}
        ) :: Ecto.Changeset.t()
  def changeset(struct, attrs) do
    struct
    |> cast(attrs, [:token, :antonyms, :synonyms, :data])
    |> validate_required([:token])
    |> cast_embed(:definitions, required: true)
  end
end
