defmodule NomDict.Dictionary.WordDefinition do
  use Ecto.Schema
  import Ecto.Changeset

  @fields [:definition, :example]

  embedded_schema do
    field(:definition, :string)
    field(:example, :string)
    timestamps()
  end

  @spec changeset(
          Ecto.Schema.t(),
          %{required(binary()) => term()} | %{required(atom()) => term()}
        ) :: Ecto.Changeset.t()
  def changeset(struct, attrs) do
    struct
    |> cast(attrs, @fields)
    |> validate_required(@fields)
  end
end
