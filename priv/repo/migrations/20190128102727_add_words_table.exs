defmodule NomDict.Repo.Migrations.AddWordsTable do
  use Ecto.Migration

  def change do
    create table(:words) do
      add(:token, :string, null: false)
      add(:definitions, {:array, :map}, default: [], null: false)
      add(:antonyms, {:array, :string}, default: [], null: false)
      add(:synonyms, {:array, :string}, default: [], null: false)
      add(:data, :map, null: false, default: %{})
      timestamps()
    end
   create unique_index(:words, :token)
  end
end
