defmodule NomDict.CLITest do
  use ExUnit.Case, async: true
  alias NomDict.CLI
  import ExUnit.CaptureIO

  test "main gives help instructions when no args are passed in" do
    assert capture_io(fn ->
      CLI.main([])
    end) == "Welcome to NomDict!\nPlease use './nom_dict' and a word to search for its definition.\n"
  end

  test "main gives definition when a word is passed in" do
    assert capture_io(fn ->
      CLI.main(["agree"])
    end) == "agree:\nDefinition 1: to have consensus\nExample 1: So happy you agree.\n"
  end

  test "protect against multi word entry" do
    assert capture_io(fn ->
       CLI.main(["agree", "help"])
     end) == "Please search for just 1 word.\n"
  end
end