defmodule NomDict.DictionaryTest do
  use NomDict.DataCase, async: true
  alias NomDict.{Dictionary, Dictionary.Word, Dictionary.WordDefinition}

  test "will retrieve word from dictionary before searching" do
    word = "test"

    {:ok, inserted_word} =
      Dictionary.create_word(%Word{}, %{
        token: word,
        definitions: [%{definition: "foo", example: "bar"}]
      })

    assert {:ok, ^inserted_word} = Dictionary.search(word)
  end

  test "will retrieve word when searching if not in dict" do
    word = "apple"

    assert nil ==
             from(w in Word, where: w.token == ^word)
             |> Repo.one()

    assert {:ok,
            %Word{
              token: "apple",
              antonyms: ["not apple"],
              synonyms: ["same apple"],
              definitions: [
                %WordDefinition{definition: "delicious", example: "Eat this delicious apple."}
              ]
            } = created_word} = Dictionary.search(word)

    assert ^created_word =
             from(w in Word, where: w.token == ^word)
             |> Repo.one()
  end

  test "not found definition will return error" do
    assert {:error, :not_found} = Dictionary.search("not-found")
  end

  test "can create word with 1 definition" do
    assert {:ok, %Word{}} =
             %Word{}
             |> Dictionary.create_word(%{
               token: "good-case",
               definitions: [
                 %{
                   example: "This is a good-case.",
                   definition: "example that passes the test"
                 }
               ]
             })
  end

  test "cannot create invalid word with bad definition keys" do
    assert {:error, %Ecto.Changeset{errors: errors}} =
             %Word{}
             |> Dictionary.create_word(%{token: "bad", definitions: [%{foo: :bar}]})
  end

  test "require token, and at least 1 definition" do
    assert {:error, %Ecto.Changeset{errors: errors}} =
             %Word{}
             |> Dictionary.create_word()

    assert Keyword.get(errors, :token) == {"can't be blank", [validation: :required]}
    assert Keyword.get(errors, :definitions) == {"can't be blank", [validation: :required]}
  end
end
