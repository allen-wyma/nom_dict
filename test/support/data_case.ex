defmodule NomDict.DataCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      alias NomDict.Repo

      import Ecto
      import Ecto.Changeset
      import Ecto.Query
      import NomDict.DataCase
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(NomDict.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(NomDict.Repo, {:shared, self()})
    end

    :ok
  end
end
